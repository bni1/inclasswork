/**********************************************
* File: AgeHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************/
// Libraries Here
#include <map>
#include <unordered_map>
#include <iterator>
#include <string>
#include <iostream>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(int argc, char** argv){
    std::map<std::string, int> ageHashOrdered = {{"matthew", 35}, {"Alfred", 72}, {"Rose", 36}, {"James", 36}};
    std::map<std::string, int>::iterator iterOr;
    std::cout << "The Ordered ages are: " << std::endl;
    for(iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++) {
        std::cout << iterOr->first << " " << iterOr->second << std::endl;
    }
    std::unordered_map<std::string, int> ageHashUnordered = {{"matthew", 35}, {"Alfred", 72}, {"Rose", 36}, {"James", 36}};
    std::unordered_map<std::string, int> :: iterator iterUn;
    std::cout << "The Unordered ages are: " << std::endl;
    for(iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); ++iterUn) {
        std::cout << iterUn->first << " " << iterUn->second << std::endl;
    }

	std::cout << "The Ordered Example: " << ageHashOrdered["matthew"] << std::endl;
    std::cout << "The Unordered Example" << ageHashUnordered["Alfred"] << std::endl;
	return 0;
}
