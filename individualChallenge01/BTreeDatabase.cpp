/**********************************************
* File: BTreeDatabase.cpp
* Author: Bo Ni
* Email: bni@Nd.edu
* 
* Implementation of the BTreeDatabase test
*
* Developed from the implementation from BTree.h
* Dr. Morrison.
**********************************************/

#include "BTree.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

struct Data {
    int data;
    int key;
    Data(int k, int d) : data(d), key(k) {};
    Data(){};
    bool operator<(const Data& rhs) {
        return key < rhs.key? true : false;
    }
    bool operator>(const Data& rhs) {
        return key > rhs.key? true : false;
    }
    bool operator==(const Data& rhs) {
        return key==rhs.key? true : false;
    }
};

/********************************************
	* Function Name  : main
	* Pre-conditions : 
	* Post-conditions: int
	*
	* main driver for the first coding challenge. 
    * Test the BTree for 1000 randomly generated numbers, and
    * test the search function for 10 randomly generated numbers.
********************************************/
int main() {
    srand(time(NULL));
    BTree<Data> database(3);      // Initialize the BTree to be of degree 3
    int i = 0;
    while (i < 1000) {
        int numFirst = rand() % 100;   // Generate the first random number between 1 and 1000
        int numSecond = rand() % 100; // Generate the second random number between 1 and 1000
        Data d = Data(numFirst, numSecond);
        cout << d.key << " " << d.data << endl;
        database.insert(d);
        ++i;
    }
    for (int i = 0; i < 10; ++i) {
        int key = rand() % 100;
        Data pseudoD = Data(key, 0);
        if(database.search(pseudoD) != nullptr) {
            BTreeNode<Data>* node = database.search(pseudoD);
            int j = 0;
            while(j < node->numKeys) {
                if(node->keys[j].key == pseudoD.key) {
                    cout << "Key: " << key << endl << "Data: " << node->keys[j].data << endl; 
                    break;
                } else {
                    ++j;
                }
            }
        } else {
            cout << "key not found" << endl;
        }
    }
    return 0;
}