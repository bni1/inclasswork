#include "BTree.h"
#include <string>

// Driver program to test above functions 
int main(int argc, char** argv) 
{ 

    BTree<std::string> t(2); // A B-Tree with minium degree 3 
 
/*
What though the odds be great or small
Old Notre Dame will win over all,
*/
 
	std::cout << "Insert: What" << std::endl;
    t.insert("What"); 
	std::cout << "Insert: though" << std::endl;	
    t.insert("though");
	std::cout << "Insert: the" << std::endl;
	t.insert("the");
	std::cout << "Insert: odds" << std::endl;
	t.insert("odds");
	std::cout << "Insert: be" << std::endl;
	t.insert("be");
	std::cout << "Insert: great" << std::endl;
	t.insert("great");
	std::cout << "Insert: or" << std::endl;
	t.insert("or");
	std::cout << "Insert: small," << std::endl;
	t.insert("small,");
	std::cout << "Insert: Old" << std::endl;
	t.insert("Old");
	std::cout << "Insert: Notre" << std::endl;
	t.insert("Notre");
	std::cout << "Insert: Dame" << std::endl;
	t.insert("Dame");
	std::cout << "Insert: will" << std::endl;
	t.insert("will");
	std::cout << "Insert: win" << std::endl;
	t.insert("win");
	std::cout << "Insert: will" << std::endl;
	t.insert("over");
	std::cout << "Insert: hsgjslaagag" << std::endl;
	t.insert("hsgjslaagag");
 
    std::cout << "Traversal of tree constructed is\n"; 
    t.traverse(); 
    std::cout << std::endl; 
  
    t.remove("hsgjslaagag"); 
    std::cout << "Traversal of tree after removing hsgjslaagag\n"; 
    t.traverse(); 
    std::cout << std::endl; 

	std::cout << "Insert: all" << std::endl;	
	t.insert("all");
    std::cout << "Traversal of tree constructed is\n"; 
    t.traverse(); 
    std::cout << std::endl; 
  
    return 0; 
} 
